import { GameState } from "GameState";
import { Game } from "phaser";

/* 
 * State control for the end of the game
 */
export class GameOverState extends Phaser.State {
    leftWall: Phaser.Graphics;
    rightWall: Phaser.Graphics;
    topWall: Phaser.Graphics;
    txtPlayAgain: Phaser.Text;
    
    constructor() {
        super();
    }

    preload() {
        this.game.state.add("GameState", GameState, false);
    }

    create() {
        let width = window.innerWidth;
        let height = window.innerHeight;
        let centerX = width / 2;
        let centerY = height / 2;
        let aspectRatio = width / height;

        let fontSize = 50 * aspectRatio;
        let style = {
			font: fontSize + "px Courier",
			fill: "#FFFFFF",
			align: "center"
        };

        this.createWalls(width, height, aspectRatio);
        
        this.txtPlayAgain = this.game.add.text(0, 0, "Click Anywhere to Play Again", style);
        this.txtPlayAgain.x = centerX - (this.txtPlayAgain.width / 2);
        this.txtPlayAgain.y = centerY - (this.txtPlayAgain.height / 2);

        this.input.onTap.addOnce(this.playAgain, this);

    }

    update() {

    }

    playAgain() {
        this.game.state.start("GameState");
    }

    //Create the game walls (boundaries)
    createWalls(width: number, height: number, aspectRatio: number) {
        let wallColor = 0x0000FF;
        let wallThickness = 10 * aspectRatio;
    
        //Create the left screen boundary (wall)
        this.leftWall = this.game.add.graphics(0, 0);
        this.leftWall.beginFill(wallColor, 1.0);
        this.leftWall.drawRect(0, 0, wallThickness, height);
        this.leftWall.endFill();
        this.game.physics.arcade.enable(this.leftWall);
        this.leftWall.body.immovable = true;
    
        //Create the right screen boundary (wall)
        this.rightWall = this.game.add.graphics(width - wallThickness, 0);
        this.rightWall.beginFill(wallColor, 1.0);
        this.rightWall.drawRect(0, 0, wallThickness, height);
        this.rightWall.endFill();
        this.game.physics.arcade.enable(this.rightWall);
        this.rightWall.body.immovable = true;
    
        //Create the top screen boundary (wall)
        this.topWall = this.game.add.graphics(0, 0);
        this.topWall.beginFill(wallColor, 1.0);
        this.topWall.drawRect(0, 0, width, wallThickness * 4);
        this.topWall.endFill();
        this.game.physics.arcade.enable(this.topWall);
        this.topWall.body.immovable = true;
    }
}