import { TileSprite } from "phaser";
import { GameOverState } from "GameOverState";
import { Paddle } from "./Sprites/Paddle";
import { Ball } from "./Sprites/Ball";
import { Block } from "./Sprites/Block";

/*
 * State control for the Breakout game
 */
export class GameState extends Phaser.State {

    //Interactive objects
    paddle: Phaser.Sprite; //player controlled paddle
    block: Phaser.Sprite;
    ball: Phaser.Sprite;
    
    //Scene objects
	leftWall: Phaser.Graphics;
	rightWall: Phaser.Graphics;
    topWall: Phaser.Graphics;

    playerInput: Phaser.CursorKeys;

    numLives: number; //Players amount of remaining lives
    blocksLeft: number; //Amount of blocks remaining to be destroyed
    ballVelocity: number;

    txtLivesLeft: Phaser.Text;
    txtBlocksLeft: Phaser.Text;

    blocks: Phaser.Group;

    isGameOver: boolean;

	constructor() {
        super();
	}

	preload() {
        this.game.state.add("GameOverState", GameOverState, false);

        //Game scale settings
        this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.game.scale.pageAlignHorizontally = true;
        this.game.scale.pageAlignVertically = true;

        //Sprite assets
		this.game.load.image("ball", "/assets/ball.png");
        this.game.load.image("paddle", "/assets/paddle.png");
        this.game.load.image("block", "/assets/block.png");
	}

	create() {
        //Game resolution variables
		let centerX = this.getGameWidth() / 2;

        //Text font style
        let fontSize = 40 * this.getScaleRatio();
        let style = {
			font: fontSize + "px Courier",
			fill: "#FFFFFF",
			align: "center"
        };

        this.isGameOver = false;
        
        this.playerInput = this.game.input.keyboard.createCursorKeys();

		//Enable the physics system for the game
		this.game.physics.startSystem(Phaser.Physics.ARCADE);

        //Create the scene objects
        this.createPaddle();
        this.createWalls();
        this.createBall();
        this.createBlocks();
        
        //Lives remaining counter
        this.numLives = 3;
        this.txtLivesLeft = this.game.add.text(centerX, 0, "Lives: " + this.numLives, style);

        //Blocks remaining counter
        this.txtBlocksLeft = this.game.add.text(0, 0, "Blocks: " + this.blocks.total, style);
	}

	update() {
        //Ball - Environment collisions
        this.checkCollisions();
        
        //Update the count for the remaining number of blocks
        this.txtBlocksLeft.setText("Blocks: " + this.blocks.total);
        this.checkLevelComplete();

        //Check if the ball has gone out of bounds
        this.ball.checkWorldBounds = true;
        this.ball.events.onOutOfBounds.add(this.loseLife, this);
        
        //Poll for player input
        this.game.input.update();

        //Move the paddle to the mouse position (on desktop)
        //or the finger press position (on mobile)
        this.paddle.position.x = this.game.input.activePointer.x;
    }

    //Scale the game objects to better suit different resolutions
    scaleObject(object) {
        let scaleRatio = this.getScaleRatio();

        object.scale.setTo(scaleRatio, scaleRatio);

    }

    //Call to the physics engine to check whether a collision has been
    //made between the game a ball and other game objects
    checkCollisions() {
        this.game.physics.arcade.collide(this.ball, this.paddle);
        this.game.physics.arcade.collide(this.ball, this.leftWall);
        this.game.physics.arcade.collide(this.ball, this.rightWall);
        this.game.physics.arcade.collide(this.ball, this.topWall);
        this.game.physics.arcade.collide(this.ball, this.blocks, this.blockHit);
    }

    //Check if there are any more blocks still active in the game,
    //if none then game is over
    checkLevelComplete() {
        if(this.blocks.total <= 0 && this.isGameOver == false) {
            this.isGameOver = true;
            this.endGame();
        }
    }

    //Collision handler to remove a block when it has been hit by the ball
    blockHit(ball, block) {
        block.kill();
    }
    
    //Remove a life or end the game when the ball goes out of bounds
    //(depending on the number of lives left)
    loseLife() {
        this.numLives--;
        this.txtLivesLeft.setText("Lives: " + this.numLives);

        //Check if the player still has any lives left
        if(this.numLives <= 0) {
            this.endGame();
        } else {
            //reset the ball
            this.createBall();
        }
    }

    //Called when the player has completed the level or 
    //when the player has ran out of lives
    endGame() {
        this.game.state.start("GameOverState");
    }

    //Create the game walls (boundaries)
    createWalls() {
        let width = this.getGameWidth();
        let height = this.getGameHeight();
        let aspectRatio = this.getScaleRatio();
        let wallColor = 0x0000FF;
        let wallThickness = 10 * aspectRatio;

		//Create the left screen boundary (wall)
		this.leftWall = this.game.add.graphics(0, 0);
		this.leftWall.beginFill(wallColor, 1.0);
        this.leftWall.drawRect(0, 0, wallThickness, height);
        this.leftWall.endFill();
        this.game.physics.arcade.enable(this.leftWall);
        this.leftWall.body.immovable = true;

		//Create the right screen boundary (wall)
		this.rightWall = this.game.add.graphics(width - wallThickness, 0);
		this.rightWall.beginFill(wallColor, 1.0);
        this.rightWall.drawRect(0, 0, wallThickness, height);
        this.rightWall.endFill();
        this.game.physics.arcade.enable(this.rightWall);
        this.rightWall.body.immovable = true;

		//Create the top screen boundary (wall)
		this.topWall = this.game.add.graphics(0, 0);
		this.topWall.beginFill(wallColor, 1.0);
        this.topWall.drawRect(0, 0, width, wallThickness * 4);
        this.topWall.endFill();
        this.game.physics.arcade.enable(this.topWall);
        this.topWall.body.immovable = true;
    }

    //Creates the game ball and sets its physics properties
    createBall() {
        let centerX = this.getGameWidth() / 2;
        let centerY = this.getGameHeight() / 2;
        let velocity = this.getBallVelocity();

		//Create the game ball
        this.ball = new Ball(this.game, centerX, centerY, velocity);
        this.game.add.existing(this.ball);
        this.scaleObject(this.ball);
    }

    //Creates the game paddle and sets its physics properties
    createPaddle() {
        let height = this.getGameHeight();
        let centerX = this.getGameWidth() / 2;
        this.paddle = new Paddle(this.game, centerX, height - 30);
        this.game.add.existing(this.paddle);
        this.scaleObject(this.paddle);        
    }

    createBlocks() {   
        let centerX = this.getGameWidth() / 2;
        let scaleRatio = this.getScaleRatio();     
        let blockOffset = 60 * scaleRatio;
        let blockRows = 3;
        let blockCols = 10;

        this.blocks = this.game.add.group();

       //Create the blocks that are to be destroyed
       for(let row = 1; row <= blockRows; row++) {
        for(let col = 0; col < blockCols; col++) {
            this.block = new Block(this.game, col * blockOffset, row * blockOffset);
            this.game.add.existing(this.block);
            this.scaleObject(this.block);
            this.blocks.add(this.block);
        }
    }
    this.blocks.x = centerX - (this.blocks.width / 2);
    }

    //Returns the width of the game screen
    getGameWidth() {
		let width = this.game.width;
		return width;
    }

    //Returns the height of the game screen
    getGameHeight() {
        let height = this.game.height;
        return height;
    }

    //Returns the aspect ratio of the game screen
    getAspectRatio() {
        let aspectRatio = this.getGameWidth() / this.getGameHeight();
        return aspectRatio;
    }

    //Returns the scale ratio to be used for objects in the game
    getScaleRatio() {
        let scaleRatio = this.getAspectRatio() * (window.devicePixelRatio / 2);

        if(window.devicePixelRatio / 2 <= 1) {
            return this.getAspectRatio();
        }

        return scaleRatio;
    }

    //Returns the velocity for the game ball
    getBallVelocity() {
        let scaleRatio = this.getScaleRatio();
        this.ballVelocity = 1000;
        let velocity = this.ballVelocity * scaleRatio;

        return velocity;
    }
}