import { Create, Graphics, Game } from "phaser";
import { GameState } from "GameState";


/*
 * Create the game window
 */
export class Breakout extends Phaser.Game {

	constructor() {
		let width = window.innerWidth * window.devicePixelRatio;
		let height = window.innerHeight * window.devicePixelRatio;

		super(width, height, Phaser.AUTO, 'content', GameState);

		console.log("Hello, World! from Phaser-" + Phaser.VERSION);
	}
}


