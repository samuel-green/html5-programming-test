import { Sprite } from "phaser";

export class Block extends Phaser.Sprite {

    constructor(game, x, y) {
        super(game, x, y, "block");

        this.game.physics.arcade.enable(this);
        this.body.immovable = true;
    }
}