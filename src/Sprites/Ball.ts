import { Sprite } from "phaser";

export class Ball extends Phaser.Sprite {

    constructor(game, x, y, velocity) {
        super(game, x, y, "ball");

		this.game.physics.arcade.enable(this);
		this.body.bounce.set(1);
        this.body.velocity.setTo(velocity, velocity);
    }
}