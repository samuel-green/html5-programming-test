import { Sprite } from "phaser";

export class Paddle extends Phaser.Sprite {

    constructor(game, x, y) {
        super(game, x, y, "paddle");

        this.game.physics.arcade.enable(this);
		this.body.immovable = true;
        this.body.collideWorldBounds = true;
    }
}